"""
TakeDown v0.0.1
===============
author: Zesheng Xing
email: zsxing@ucdavis.edu

This Python project is to help people search on some client hosting contents that potential violate the their copyright.
"""

from . import controller
from . import client

